from django.contrib import admin
from .models import Grades,Sections,Subjects

# Register your models here.

admin.site.register(Grades)
admin.site.register(Sections)
admin.site.register(Subjects)
