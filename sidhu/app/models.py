from django.db import models

# Create your models here.

class Grades(models.Model):
    cls = models.CharField(max_length=15)

    def __str__(self):
        return self.cls

class Sections(models.Model):
    sec = models.ForeignKey(Grades,on_delete=models.CASCADE)
    section = models.CharField(max_length=10)

    def __str__(self):
        return self.section

class Subjects(models.Model):
    cls = models.ForeignKey(Grades,on_delete=models.CASCADE)
    sect = models.ForeignKey(Sections,on_delete=models.CASCADE)
    subject = models.CharField(max_length=50)

    def __str__(self):
        return self.subject
